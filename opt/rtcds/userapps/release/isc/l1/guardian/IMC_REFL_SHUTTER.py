#! /usr/bin/env python

from guardian import GuardState, GuardStateDecorator
import time

request = 'IDLE'
nominal = 'CHECKING'


def is_locked():
    return ezca['IMC-MC2_TRANS_SUM_INMON'] >= 50*ezca['IMC-PWR_IN_OUTPUT']

def too_much_power():
    return ezca['IMC-PWR_IN_OUTPUT'] > 50


class INIT(GuardState):
    index = 0
    def main(self):
        return True


class IDLE(GuardState):
    index = 5
    def run(self):
        return True


class CHECKING(GuardState):
    index = 10
    def run(self):
        if is_locked():
            return
        elif too_much_power():
            log('Too much power')
            if ezca['SYS-MOTION_C_SHUTTER_A_STATE'] == 0:
                log('Shuttering')
                ezca['SYS-MOTION_C_SHUTTER_A_CLOSE'] = 1
        else:
            log('Power under shutter threshold')
            if ezca['SYS-MOTION_C_SHUTTER_A_STATE'] == 1:
                log('Opening')
                ezca['SYS-MOTION_C_SHUTTER_A_OPEN'] = 1
        return

edges = [
    ('INIT','IDLE'),
    ('IDLE','CHECKING')
]
